//
//  TBFetchTableView.swift
//  TeaBreak
//
//  Created by Adarsh Roy on 27/07/16.
//  Copyright © 2016 Indusnet Technology. All rights reserved.
//

import UIKit
import CoreData

class TBFetchTableView: UITableView,NSFetchedResultsControllerDelegate,UITableViewDataSource,UITableViewDelegate {
    
    internal var cellForTableViewAtIndexPath:((tableView: UITableView, indexPath: NSIndexPath ,dataSource: AnyObject) -> (UITableViewCell))?
    
    var sortDescriptorKeyPath: String?
    var canCommitCellEditingStyle : Bool? = false
    
    var entity: String?{
        
        didSet{
            do{
                try fetchedResultsController!.performFetch()
            }
            catch let error as NSError
            {
                debugPrint("Error fetching.... \(error)")
            }
            self.delegate = self
            self.dataSource = self
        }
    }
    
    lazy var fetchedResultsController : NSFetchedResultsController? = {
        
        let fetchRequest = NSFetchRequest(entityName: self.entity!)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: self.sortDescriptorKeyPath!, ascending: true)]
        var tempFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: manegedObjectContext, sectionNameKeyPath: nil, cacheName: "Root")
        tempFetchedResultsController.delegate = self
        return tempFetchedResultsController
    }()
    
    internal func configureFetchResultsCollectionView(entity: String!, and sortDescriptorKeyPath: String!){
        
        self.sortDescriptorKeyPath = sortDescriptorKeyPath
        self.entity = entity
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return (fetchedResultsController?.sections?.count)!
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if fetchedResultsController?.sections?[0].numberOfObjects > 0 {
            return (fetchedResultsController?.sections?[0].numberOfObjects)!
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        return self.cellForTableViewAtIndexPath!(tableView: tableView, indexPath: indexPath, dataSource: (fetchedResultsController?.objectAtIndexPath(indexPath))!)
    }
    
    func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        return true
    }
    
    func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath)
    {
        if editingStyle == .Delete {
            let context = self.fetchedResultsController!.managedObjectContext
            context.deleteObject(self.fetchedResultsController!.objectAtIndexPath(indexPath) as! Customer)
            do {
                try context.save()
            }catch {(error)
                
            }
        }
    }
    
    func controllerWillChangeContent(controller: NSFetchedResultsController)
    {
        self.beginUpdates()
    }
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?)
    {
        switch type {
        case .Insert:
            self.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        case .Delete:
            self.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Update:
            self.reloadRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
        case .Move:
            self.deleteRowsAtIndexPaths([indexPath!], withRowAnimation: .Fade)
            self.insertRowsAtIndexPaths([newIndexPath!], withRowAnimation: .Fade)
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController)
    {
        self.endUpdates()
    }
}
