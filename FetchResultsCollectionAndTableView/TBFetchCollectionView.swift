 //
 //  TBFetchCollectionView.swift
 //  TeaBreak
 //
 //  Created by Adarsh Roy on 27/07/16.
 //  Copyright © 2016 Indusnet Technology. All rights reserved.
 //
 
 import UIKit
 import CoreData
 
 class TBFetchCollectionView: UICollectionView, NSFetchedResultsControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var blockOperations = [NSBlockOperation]()
    
    var entity: String?{
        
        didSet{
            do{
                try self.fetchedResultsController!.performFetch()
            }
            catch let error as NSError
            {
                debugPrint("Error fetching.... \(error)")
            }
            self.delegate = self
            self.dataSource = self
        }
    }
    
    var sortDescriptorKeyPath: String?
    
    lazy var fetchedResultsController : NSFetchedResultsController? = {
        let fetchRequest = NSFetchRequest(entityName: self.entity!)
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: self.sortDescriptorKeyPath!, ascending: true)]
        fetchRequest.fetchBatchSize = 20
        var tempFetchedResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: manegedObjectContext, sectionNameKeyPath: nil, cacheName: nil)
        tempFetchedResultsController.delegate = self
        return tempFetchedResultsController
    }()
    
    internal var cellForCollectionViewAtIndexPath:((collectionView: UICollectionView!, indexPath: NSIndexPath!, dataSource: AnyObject!) -> (UICollectionViewCell))?
    internal var sizeForCollectionViewAtIndexPath:((collectionView: UICollectionView!, indexPath: NSIndexPath!, dataSource: AnyObject!) -> (CGSize))?
    
    internal func configureFetchResultsCollectionView(entity: String!, andsortDescriptorKeyPath sortDescriptorKeyPath: String!){
        
        self.sortDescriptorKeyPath = sortDescriptorKeyPath
        self.entity = entity
    }
    
    //MARK:- UICollectionViewDataSource Methods -
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int
    {
        return (fetchedResultsController?.sections?.count)!
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        if fetchedResultsController?.sections?[0].numberOfObjects > 0 {
            return (fetchedResultsController?.sections?[0].numberOfObjects)!
        }
        return 0
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        return self.cellForCollectionViewAtIndexPath!(collectionView: collectionView ,indexPath: indexPath, dataSource: fetchedResultsController?.objectAtIndexPath(indexPath))
    }
    
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize
    {
        return self.sizeForCollectionViewAtIndexPath!(collectionView: collectionView ,indexPath: indexPath, dataSource: fetchedResultsController?.objectAtIndexPath(indexPath))
    }
    
    //MARK:- NSFetchedResultsControllerDelegate Methods -
    
    func controller(controller: NSFetchedResultsController, didChangeObject anObject: AnyObject, atIndexPath indexPath: NSIndexPath?, forChangeType type: NSFetchedResultsChangeType, newIndexPath: NSIndexPath?)
    {
        
        switch type {
        case .Insert:
            blockOperations.append(NSBlockOperation(block: {
                self.insertItemsAtIndexPaths([newIndexPath!])
            }))
        case .Delete:
            blockOperations.append(NSBlockOperation(block: {
                self.deleteItemsAtIndexPaths([indexPath!])
            }))
        case .Update:
            blockOperations.append(NSBlockOperation(block: {
                self.reloadItemsAtIndexPaths([indexPath!])
            }))
        case .Move:
            blockOperations.append(NSBlockOperation(block: {
                self.deleteItemsAtIndexPaths([indexPath!])
                self.insertItemsAtIndexPaths([newIndexPath!])
            }))
        }
    }
    
    func controllerDidChangeContent(controller: NSFetchedResultsController)
    {
        self.performBatchUpdates({
            for operation in self.blockOperations {
                operation.start()
            }
        }) { (finished) in
            //Done
        }
    }
 }
