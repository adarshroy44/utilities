//
//  LocationViewController.swift
//  SwiftTestAll
//
//  Created by Adarsh Roy on 09/08/16.
//  Copyright © 2016 Indusnet Technology. All rights reserved.
//

import UIKit
import CoreLocation

class LocationViewController: UIViewController,CLLocationManagerDelegate
{
    @IBOutlet weak var lblLat: UILabel!
    @IBOutlet weak var lblLong: UILabel!
    @IBOutlet weak var lblSpeed: UILabel!
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        //mandatory settings to be done
        locationManager.distanceFilter = kCLDistanceFilterNone
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self;
        
        /*
         In INFO.PLIST file -
         
         Make sure to add NSLocationWhenInUseUsageDescription = "some message" (when u do locationManager.requestWhenInUseAuthorization())
         OR
         Make sure to add NSLocationAlwaysUsageDescription = "some message" (when u do locationManager.requestAlwaysAuthorization())
         */
    }
    
    @IBAction func btnAction(sender: AnyObject) {
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        else
        {
            showAlert("location is off !!")
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(message:String) {
        let alert = UIAlertController(title: "Alert", message:message , preferredStyle: UIAlertControllerStyle.Alert)
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    //MARK: -  CLLocationManagerDelegate delegate functions
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //        locationManager.stopUpdatingLocation()
        //        let userLocation:CLLocation = locations[0]
        let userLocation:CLLocation = locations.last! // Get the user location in CLLocation object
        
        let long = userLocation.coordinate.longitude;
        let lat = userLocation.coordinate.latitude;
        let speed = userLocation.speed; // Get the speed from location object
        
        print("lat- \(lat),    long- \(long),    speed- \(speed)")
        lblLat.text = "\(lat)"
        lblLong.text = "\(long)"
        lblSpeed.text = "\(speed)"
    }
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        showAlert("Location fetching failed.")
    }
    
}
