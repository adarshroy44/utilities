//
//  ARTextField.swift
//
//
//  Created by Adarsh Roy on 9/1/16.
//  Copyright © 2016 Adarsh. All rights reserved.
//

import UIKit
@IBDesignable
class ARTextField: UITextField {
    var imageViewIcon = UIImageView(frame: CGRect(x: 0, y: 0, width: 45, height: 50))
    var resultEdgeInset: UIEdgeInsets = UIEdgeInsets()
    
    @IBInspectable var leftPaddingForText: CGFloat = 0 {
        didSet {
            resultEdgeInset = UIEdgeInsets(top: 0, left: CGRectGetWidth(imageViewIcon.frame)+leftPaddingForText, bottom: 0, right: 5);
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            self.clipsToBounds = true
        }
    }
    
    @IBInspectable var image: UIImage? {
        didSet {
            self.leftViewMode = .Always
            imageViewIcon.image = image
            self.leftView = imageViewIcon
        }
    }
    
    override func textRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, resultEdgeInset)
    }
    
    override func placeholderRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, resultEdgeInset)
    }
    
    override func editingRectForBounds(bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, resultEdgeInset)
    }
}